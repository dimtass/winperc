#include "winperc.h"

WinPerc::WinPerc(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags), m_tabIndex(TAB_INTRO)
{
	ui.setupUi(this);

	//Create signals
	connect(ui.btnNext, SIGNAL(clicked()), this, SLOT(OnNextTab()));
	connect(ui.btnBack, SIGNAL(clicked()), this, SLOT(OnPrevTab()));

	Init();
}

WinPerc::~WinPerc()
{

}

void WinPerc::Init()
{
	//Prepare intro text
	ui.lblInfoText->setText( "WinPerc is just a utility to manage and create automatic email reports for "
		"Dell Perc/LSI controllers that are compatible with MegaCLI command utility. WinPerc uses the windows version of sendmail "
		"and the windows native scheduler.\n\n"
		"Warning:\n The only changes that this utility does is to create the folder c:\\winperc to your hard drive and add "
		"some registry values regarding the reports you create. If you want to automatically clear these changes then just "
		"select \"Remove All\" in the next step and then quit. This will delete all WinPerc entries from the windows scheduler.\n\n"
		"By using automatic reports you will be warned if something if there's a problem with your controller or disks array "
		"so maybe you have some time to rescue your data before everything is fucked up.\n\n"
		"Jaco 2/2011 - v0.1b");

	//Prepare tabs
	for (int i=TAB_INTRO; i<=TAB_END; i++) ui.tabWidget->setTabEnabled(i, false);
	ui.tabWidget->setCurrentIndex(m_tabIndex);
	ui.tabWidget->setTabEnabled(m_tabIndex, true);

	//prepare TAB_ACTION
	ui.radioNew->setChecked(true);

	//prepare TAB_NEW
	ui.txtEmailUsername->setText("@gmail.com");
	connect(ui.btnEmailTest, SIGNAL(clicked()), this, SLOT(OnTestEmail()));
	ui.btnEmailTest->setText("Test settings");

	QStringList reportList;
	reportList << "Adapter Count" << "Adapter Info" << "VDrive Info" << "Custom";
	ui.cbxReportType->addItems(reportList);
	ui.txtReportCommand->setEnabled(false);

	connect(ui.cbxReportType, SIGNAL(currentIndexChanged(QString)), this, SLOT(OnSelectReportType(QString)));
	m_reportCmd = "-AdpCount";
	ui.txtReportCommand->setText(m_reportCmd);

	connect(ui.btnReportTest, SIGNAL(clicked()), this, SLOT(OnTestReport()));

	connect(ui.btnTestEmailReport, SIGNAL(clicked()), this, SLOT(OnTestEmailReport()));
	//%windir%\system32\taskschd.msc /s

	//Scheduler tab
	ui.groupBoxStatus->hide();
	ui.selTime->setTime(QTime::currentTime());
	ui.selDaysPeriod->setValue(1);
	ui.listSchedulerTasks->clear();
	connect(ui.btnOpenScheduler, SIGNAL(clicked()), this, SLOT(OnOpenScheduler()));
	connect(ui.btnDeleteTask, SIGNAL(clicked()), this, SLOT(OnDeleteSelectedTask()));
	connect(ui.btnCreateTask, SIGNAL(clicked()), this, SLOT(OnCreateScedulerTask()));

	//Remove Tab
	ui.groupBoxRemoveStatus->hide();
	ui.listSchedulerTasks->clear();
	connect(ui.btnRemove, SIGNAL(clicked()), this, SLOT(OnDeleteFiles()));

}

bool WinPerc::FileCopy(QString sourceFileName, QString destFileName)
{
	bool resp = true;

	//copy header.txt+result.txt email.txt
	QProcess process;
	QStringList args;

	sourceFileName.replace("/", "\\");
	destFileName.replace("/", "\\");

	args << "/C" << "copy" << sourceFileName << destFileName;
	process.start("cmd.exe", args);
	process.waitForStarted(200);
	if (!process.waitForFinished(20000) ) {
		ShowMsg("Failed to copy from " + sourceFileName + " to " + destFileName, MSG_ERROR);
		resp = false;
	}
	return(resp);
}

bool WinPerc::DeleteFile(QString fileName) 
{
	bool resp = true;

	QProcess process;
	QStringList args;

	fileName.replace("/", "\\");

	args << "/C" << "del" << "/F" << "/S" << "/Q" << fileName;
	process.start("cmd.exe", args);
	process.waitForStarted(200);
	if (!process.waitForFinished(2000) ) {
		ShowMsg("Failed to delete file " + fileName, MSG_ERROR);
		resp = false;
	}
	return(resp);
}


bool WinPerc::DeleteFolder(QString folderName)
{
	bool resp = true;

	QProcess process;
	QStringList args;

	folderName.replace("/", "\\");

	args << "/C" << "rmdir" << "/S" << "/Q" << folderName;
	process.start("cmd.exe", args);
	process.waitForStarted(200);
	if (!process.waitForFinished(20000) ) {
		ShowMsg("Failed to delete file " + folderName, MSG_ERROR);
		resp = false;
	}
	return(resp);
}


void WinPerc::UpdateScheduleTasks()
{
	setCursor(QCursor (Qt::WaitCursor));
	ui.listSchedulerTasks->clear();
	QStringList tasks = GetCurrentScheduleTasks();
	for each (QString task in tasks) {
		ui.listSchedulerTasks->addItem(task);
	}
	setCursor(QCursor (Qt::ArrowCursor));
}

void WinPerc::UpdateInstalledComponents()
{
	setCursor(QCursor (Qt::WaitCursor));
	ui.listFoundComponents->clear();
	QStringList components = FindInstalledComponents();
	for each (QString component in components) {
		ui.listFoundComponents->addItem(component);
	}
	setCursor(QCursor (Qt::ArrowCursor));
}

QStringList WinPerc::FindInstalledComponents(void)
{
	QStringList components;
	bool enableButton = false;

	ui.chkRemoveReports->setChecked(false);
	ui.chkRemoveFiles->setChecked(false);
	ui.chkRemoveReports->setEnabled(false);
	ui.chkRemoveFiles->setEnabled(false);

	components << GetCurrentScheduleTasks();
	if (components[0].contains("No WinPerc tasks found")) {
		components.clear();
	}
	else {
		components.clear();
		components << "Active WinPerc scheduler task found";
		ui.chkRemoveReports->setChecked(true);
		ui.chkRemoveReports->setEnabled(true);
		enableButton = true;
	}
	if (QFile::exists("C:/WinPerc/header.txt")) components << "C:/WinPerc/header.txt";
	if (QFile::exists("C:/WinPerc/libeay32.dll")) components << "C:/WinPerc/libeay32.dll";
	if (QFile::exists("C:/WinPerc/ssleay32.dll")) components << "C:/WinPerc/ssleay32.dll";
	if (QFile::exists("C:/WinPerc/sendmail.exe")) components << "C:/WinPerc/sendmail.exe";
	if (QFile::exists("C:/WinPerc/sendmail.ini")) components << "C:/WinPerc/sendmail.ini";
	if (QFile::exists("C:/WinPerc/report.cmd")) components << "C:/WinPerc/report.cmd";
	if (QFile::exists("C:/WinPerc/MegaCli.exe")) components << "C:/WinPerc/MegaCli.exe";
	
	QDir tmpDir("C:/WinPerc");
	if (tmpDir.exists()) {
		ui.chkRemoveFiles->setChecked(true);
		ui.chkRemoveFiles->setEnabled(true);
		enableButton = true;
	}

	if (enableButton) {
		ui.btnRemove->setEnabled(true);
	}
	else {
		ui.btnRemove->setEnabled(false);
		components.clear();
		components << "No installed components found!";
	}

	return components;
}

QStringList WinPerc::GetCurrentScheduleTasks(void)
{
	QString fileName = "tasks.txt";
	QStringList tasks;

	tasks.clear();

	QProcess process;
	process.start("cmd.exe", QStringList() << "/C" << "%windir%/system32/schtasks.exe" << "/Query" << ">>" << fileName);
	process.waitForStarted(500);
	qDebug() << "schtasks.exe started";
	if (!process.waitForFinished(20000) ) {
		ShowMsg("Failed to get current scheduler's tasks. :p", MSG_ERROR);
	}

	QFile file(fileName);

	if (!file.isOpen()) {
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			ShowMsg("Could not open report text. Run as administrator may solve this problem?",
				MSG_ERROR);
			qDebug() << "Error: " << file.errorString();
			file.close();
		}
		else {
			QTextStream rs(&file);	//read stream
			QString line = rs.readLine();
			while (!line.isNull()) {
				if (line.contains("WinPerc")) {
					tasks << line;
				}
				line = rs.readLine();
			}
			file.close();
		}
	}
	CleanFiles(QStringList() << fileName);

	if (tasks.isEmpty()) tasks << "No WinPerc tasks found";

	return(tasks);
}


bool WinPerc::CreateCmd(QString cmdPath)
{
	QFile rFile(":/Files/Resources/tools/report.cmd");
	QFile wFile("report.cmd");
	bool resp = false;

	cmdPath.replace("/", "\\");
	qDebug() << "cmdPath: " << cmdPath;


	if (!rFile.isOpen()) {
		if (!rFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			ShowMsg("Could not edit needed files for testing. Run as administrator may solve this problem.",
				MSG_ERROR);
			qDebug() << "Error: " << rFile.errorString();
			rFile.close();
		}
		else {
			if (wFile.open(QIODevice::ReadWrite | QIODevice::Text) ) {
				QTextStream rs(&rFile);	//read stream
				QTextStream ws(&wFile);	//write stream
				QString buffer = rs.readAll();
				//Change WINPERC_PATH
				buffer.replace("WINPERC_PATH", cmdPath);
				buffer.replace("MEGACLI_COMMAND", "MegaCli.exe " + m_reportCmd + " >> result.txt");

				ws << buffer;
				ws.flush();
				wFile.close();
			}
			else {
				ShowMsg("Could not create header.txt. Run as administrator may solve this problem.",
					MSG_ERROR);
				qDebug() << "Error: " << wFile.errorString();
				wFile.close();

			}
			rFile.close();
		}
	}
	return(resp);

	return(resp);
}

bool WinPerc::CreateSendMail(void) 
{
	QFile tmpFile;
	bool resp;

	QString currPath = QDir::currentPath();
	resp = tmpFile.copy(":/Files/Resources/tools/sendmail.exe", currPath + "/sendmail.exe");
	tmpFile.setPermissions(currPath + "/sendmail.exe", QFile::WriteUser | QFile::ExeUser);
	resp = tmpFile.copy(":/Files/Resources/tools/ssleay32.dll", currPath + "/ssleay32.dll");
	tmpFile.setPermissions(currPath + "/ssleay32.dll", QFile::WriteUser | QFile::ExeUser);
	resp = tmpFile.copy(":/Files/Resources/tools/libeay32.dll", currPath + "/libeay32.dll");
	tmpFile.setPermissions(currPath + "/libeay32.dll", QFile::WriteUser | QFile::ExeUser);

	return(resp);
}

bool WinPerc::CreateMegaCLI(void) {
	QFile tmpFile;
	bool resp = false;

	QString currPath = QDir::currentPath();
	resp = tmpFile.copy(":/Files/Resources/tools/MegaCli.exe", currPath + "/MegaCli.exe");
	tmpFile.setPermissions(currPath + "/MegaCli.exe", QFile::WriteUser | QFile::ExeUser);

	return(resp);
}

bool WinPerc::CreateHeaderTxt(QString emailTo, QString emailSubj)
{
	QFile rFile(":/Files/Resources/tools/header.txt");
	QFile wFile("header.txt");
	bool resp = false;

	if (!rFile.isOpen()) {
		if (!rFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			ShowMsg("Could not edit needed files for testing. Run as administrator may solve this problem.",
				MSG_ERROR);
			qDebug() << "Error: " << rFile.errorString();
			rFile.close();
		}
		else {
			if (wFile.open(QIODevice::ReadWrite | QIODevice::Text) ) {
				QTextStream rs(&rFile);	//read stream
				QTextStream ws(&wFile);	//write stream
				QString buffer = rs.readAll();
				//Change EMAIL_SENDER
				buffer.replace("EMAIL_SENDER", emailTo);
				//Change EMAIL_SUBJ
				buffer.replace("EMAIL_SUBJ", emailSubj);
				//re-write file;

				ws << buffer;
				ws.flush();
				wFile.close();
			}
			else {
				ShowMsg("Could not create header.txt. Run as administrator may solve this problem.",
					MSG_ERROR);
				qDebug() << "Error: " << wFile.errorString();
				wFile.close();

			}
			rFile.close();
		}
	}
	return(resp);
}

bool WinPerc::CreateSendMainIni(QString smtpServer, QString smptPort, QString username, QString password)
{
	QFile rFile(":/Files/Resources/tools/sendmail.ini");
	QFile wFile("sendmail.ini");
	bool resp = false;

	if (!rFile.isOpen()) {
		if (!rFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			ShowMsg("Could not edit needed files for testing. Run as administrator may solve this problem.",
				MSG_ERROR);
			qDebug() << "Error: " << rFile.errorString();
			rFile.close();
		}
		else {
			if (wFile.open(QIODevice::ReadWrite | QIODevice::Text) ) {
				QTextStream rs(&rFile);	//read stream
				QTextStream ws(&wFile);	//write stream
				QString buffer = rs.readAll();
				//Change SMTP_SERVER
				buffer.replace("SMTP_SERVER", smtpServer, Qt::CaseSensitive);
				//Change SMTP_PORT
				buffer.replace("SMTP_PORT", smptPort, Qt::CaseSensitive);
				//Change EMAIL_USERNAME
				buffer.replace("EMAIL_USERNAME", username, Qt::CaseSensitive);
				//Change EMAIL_PASSWORD
				buffer.replace("EMAIL_PASSWORD", password, Qt::CaseSensitive);
				//re-write file;

				ws << buffer;
				ws.flush();
				wFile.close();
			}
			else {
				ShowMsg("Could not create sendmail.ini. Run as administrator may solve this problem.",
					MSG_ERROR);
				qDebug() << "Error: " << wFile.errorString();
				wFile.close();

			}
			rFile.close();
		}
	}
	return(resp);
}

bool WinPerc::CleanFiles(QStringList files)
{
	bool resp = false;

	for each (QString file in files) {
		QFile::remove(file);
	}
	return(resp);
}


void WinPerc::ShowMsg(QString msg, en_messageType type)
{

	QMessageBox msgBox; 
	if (type == MSG_INFO)
		msgBox.setText("Info.");
	else if (type == MSG_WARNING)
		msgBox.setText("Warning!");
	else if (type == MSG_ERROR)
		msgBox.setText("An error occurred");

	msgBox.setInformativeText(msg);
	msgBox.exec();
}

//SLOTS
void WinPerc::OnSelectReportType(QString reportType)
{
	if (reportType == "Custom") {
		ui.txtReportCommand->setEnabled(true);
		ui.txtReportCommand->setText("");
	}
	else {
		ui.txtReportCommand->setEnabled(false);
		if (reportType == "Adapter Count") {
			m_reportCmd = "-AdpCount";
			ui.txtReportCommand->setText(m_reportCmd);
		}
		else if (reportType == "Adapter Info") {
			m_reportCmd = "-AdpAllInfo -aALL";
			ui.txtReportCommand->setText(m_reportCmd);
		}
		else if (reportType == "VDrive Info") {
			m_reportCmd = "-LDInfo -Lall -aALL";
			ui.txtReportCommand->setText(m_reportCmd);
		}
	}
}

void WinPerc::OnNextTab()
{
	if (++m_tabIndex == TAB_END) --m_tabIndex;
	for (int i=TAB_INTRO; i<TAB_END; i++) ui.tabWidget->setTabEnabled(i, false);

	if (m_tabIndex == TAB_NEW_REPORT) {
		if (ui.radioRemoveAll->isChecked()) {
			UpdateInstalledComponents();
			m_tabIndex = TAB_REMOVE;
		}
	}
	else if (m_tabIndex == TAB_SCHEDULER) {
		//scan for schedules
		UpdateScheduleTasks();
	}
	else if (m_tabIndex == TAB_REMOVE) {
		if (ui.radioNew->isChecked()) --m_tabIndex;
	}
	ui.tabWidget->setCurrentIndex(m_tabIndex);
	ui.tabWidget->setTabEnabled(m_tabIndex, true);
	
}

void WinPerc::OnPrevTab()
{
	for (int i=TAB_INTRO; i<=TAB_END; i++) ui.tabWidget->setTabEnabled(i, false);
	if (m_tabIndex == TAB_REMOVE) {
		m_tabIndex = TAB_ACTION;
		ui.tabWidget->setCurrentIndex(m_tabIndex);
		ui.tabWidget->setTabEnabled(m_tabIndex, true);
		
	}
	else {
		if (--m_tabIndex < 0) m_tabIndex = TAB_INTRO;
		ui.tabWidget->setCurrentIndex(m_tabIndex);
		ui.tabWidget->setTabEnabled(m_tabIndex, true);
	}
}


void WinPerc::OnHideStatusGroup()
{
	ui.groupBoxStatus->hide();
	ui.groupBoxRemoveStatus->hide();
}

void WinPerc::OnCreateScedulerTask()
{
	setCursor(QCursor (Qt::WaitCursor));
	ui.groupBoxStatus->show();

	ui.listSchedulerTasks->setCurrentRow(0);
	OnDeleteSelectedTask();

	bool bResult;
	//Create folder
	ui.prgStatus->setValue(5);
	ui.lblStatus->setText("Creating C:\\WinPerk folder");
	QDir appDir;
	QFile tmpFile;
	appDir.mkdir("C:/WinPerc");

	QString appPath = "C:/WinPerc";
	QString currPath = QDir::currentPath();

	//Create header.txt
	ui.prgStatus->setValue(10);
	ui.lblStatus->setText("Creating email headers");
	CreateHeaderTxt(ui.txtEmailReceiver->text(), "WinPerc Report");
	FileCopy("header.txt", appPath + "/header.txt");

	//Create sendmail.ini
	ui.prgStatus->setValue(15);
	ui.lblStatus->setText("Creating sendmail ini file");
	CreateSendMainIni(ui.txtEmailSmtp->text(), ui.txtEmailPort->text(), ui.txtEmailUsername->text(), ui.txtEmailPassword->text());
	FileCopy("sendmail.ini", appPath + "/sendmail.ini");

	//Create sendmail
	ui.prgStatus->setValue(20);
	ui.lblStatus->setText("Creating sendmail application data");
	CreateSendMail();
	ui.prgStatus->setValue(25);
	ui.lblStatus->setText("Copying sendmail.exe");
	FileCopy("sendmail.exe", appPath + "/sendmail.exe");
	ui.prgStatus->setValue(30);
	ui.lblStatus->setText("Copying libeay32.dll");
	FileCopy("libeay32.dll", appPath + "/libeay32.dll");
	ui.prgStatus->setValue(35);
	ui.lblStatus->setText("Copying ssleay32.dll");
	FileCopy("ssleay32.dll", appPath + "/ssleay32.dll");

	//Create MegaCLI
	ui.prgStatus->setValue(40);
	ui.lblStatus->setText("Creating MegaCLI application data");
	CreateMegaCLI();
	ui.prgStatus->setValue(45);
	ui.lblStatus->setText("Copying MegaCLI.exe");
	FileCopy("MegaCli.exe", appPath + "/MegaCli.exe");

	//Create report.cdm
	ui.prgStatus->setValue(50);
	ui.lblStatus->setText("Creating report script");
	CreateCmd(appPath);
	FileCopy("report.cmd", appPath + "/report.cmd");



	// schtasks /Create /SC daily /TN WinPerc /ST 23:00:00 /TR c:\test.cmd
	QString taskTime;
	QString dates;
	QStringList args;


	ui.prgStatus->setValue(50);
	ui.lblStatus->setText("Registering the script in scheduler...");

	args << "/C" << "%windir%/system32/schtasks.exe" << "/Create" << "/SC";

	int days = ui.selDaysPeriod->value();
	if (days == 1) {
		args << "DAILY";
	}
	else if (days == 2) {
		args << "/MONTHLY" << "/D" << "1,3,5,7,9,11,13,15,17,19,21,23,25,27,29";
	}
	else if (days == 3) {
		args << "/MONTHLY" << "/D" << "1,4,7,10,13,16,19,22,25,28,31";
	}
	else if (days == 4) {
		args << "/MONTHLY" << "/D" << "1,5,9,13,17,21,25,29";
	}
	else if (days == 5) {
		args << "/MONTHLY" << "/D" << "1,6,11,16,21,26";
	}
	else if (days == 6) {
		args << "/MONTHLY" << "/D" << "1,7,13,19,25,31";
	}
	else if (days == 7) {
		args << "/MONTHLY" << "/D" << "1,8,15,22,29";
	}
	args << "/TN" << "WinPerc" << "/ST" << ui.selTime->text() << "/TR" << "C:\\WinPerc\\report.cmd";

	qDebug() << args;

	QProcess process;
	process.start("cmd.exe", args);
	process.waitForStarted(1500);
	if (!process.waitForFinished(20000) ) {
		ShowMsg("Failed to create the WinPerc task. :p", MSG_ERROR);
	}

	ui.prgStatus->setValue(87);
	ui.lblStatus->setText("Updating the scheduler list...");
	UpdateScheduleTasks();


	ui.prgStatus->setValue(98);
	ui.lblStatus->setText("Cleaning temporary files...");

	CleanFiles(QStringList() << "sendmail.exe" << "header.txt" << "sendmail.ini" << "ssleay32.dll" << 
		"libeay32.dll" << "report.cmd" << "MegaCli.exe" << "MegaSAS.log");


	ui.prgStatus->setValue(100);
	ui.lblStatus->setText("Finished");

	QTimer::singleShot(3000, this, SLOT(OnHideStatusGroup()));
	setCursor(QCursor (Qt::ArrowCursor));
}


void WinPerc::OnDeleteFiles()
{
	setCursor(QCursor (Qt::WaitCursor));
	ui.groupBoxRemoveStatus->show();

	if (ui.chkRemoveFiles->isChecked()) {
		ui.prgRemoveStatus->setValue(10);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\sendmail.exe");
		DeleteFile("C:\\WinPerc\\sendmail.exe");

		ui.prgRemoveStatus->setValue(20);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\sendmail.ini");
		DeleteFile("C:\\WinPerc\\sendmail.ini");

		ui.prgRemoveStatus->setValue(30);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\ssleay32.dll");
		DeleteFile("C:\\WinPerc\\ssleay32.dll");

		ui.prgRemoveStatus->setValue(40);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\libeay32.dll");
		DeleteFile("C:\\WinPerc\\libeay32.dll");

		ui.prgRemoveStatus->setValue(50);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\report.cmd");
		DeleteFile("C:\\WinPerc\\report.cmd");

		ui.prgRemoveStatus->setValue(60);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\MegaCli.exe");
		DeleteFile("C:\\WinPerc\\MegaCli.exe");

		ui.prgRemoveStatus->setValue(70);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\MegaSAS.log");
		DeleteFile("C:\\WinPerc\\MegaSAS.log");

		ui.prgRemoveStatus->setValue(80);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc\\header.txt");
		DeleteFile("C:\\WinPerc\\header.txt");

		//Delete folder
		ui.prgRemoveStatus->setValue(90);
		ui.lblRemoveStatus->setText("Deleting C:\\WinPerc folder");
		DeleteFolder("C:\\WinPerc");
	}

	//Delete tasks
	if (ui.chkRemoveReports->isChecked()) {

		ui.prgRemoveStatus->setValue(98);
		ui.lblRemoveStatus->setText("Deleting scheduler tasks...");

		QProcess process;
		process.start("cmd.exe", QStringList() << "/C" << "%windir%/system32/schtasks.exe" << "/Delete" << "/TN" << "WinPerc" << "/f");
		process.waitForStarted(500);
		if (!process.waitForFinished(3000) ) {
			ShowMsg("Failed to delete the selected WinPerc task. :p", MSG_ERROR);
		}
	}

	QTimer::singleShot(3000, this, SLOT(OnHideStatusGroup()));


	ui.prgRemoveStatus->setValue(100);
	ui.lblRemoveStatus->setText("Finished");

	UpdateInstalledComponents();

	setCursor(QCursor (Qt::ArrowCursor));
}



void WinPerc::OnOpenScheduler()
{
	QProcess * process = new QProcess();
	process->start("cmd.exe", QStringList() << "/C" << "%windir%/system32/taskschd.msc /s");
	process->waitForStarted(500);
	qDebug() << "taskschd.msc started";
}

void WinPerc::OnDeleteSelectedTask()
{
	// schtasks /Delete /TN "WinPerc" /f
	QListWidgetItem * currentItem = ui.listSchedulerTasks->currentItem();
	if (currentItem == NULL) {
		ShowMsg("Select a task from the above list.", MSG_WARNING);
		return;
	}
	if (currentItem->text().contains("No WinPerc tasks found")) {
		ShowMsg("There aren't any WinPerc tasks to delete", MSG_WARNING);
	}
	else if (currentItem->text().contains("WinPerc")) {
		QProcess process;
		process.start("cmd.exe", QStringList() << "/C" << "%windir%/system32/schtasks.exe" << "/Delete" << "/TN" << "WinPerc" << "/f");
		process.waitForStarted(500);
		if (!process.waitForFinished(20000) ) {
			ShowMsg("Failed to delete the selected WinPerc task. :p", MSG_ERROR);
		}
		UpdateScheduleTasks();
	}
}


void WinPerc::OnTestEmailReport()
{
	ui.btnTestEmailReport->setText("Please wait...");
	ui.btnTestEmailReport->update();

	ShowMsg("Now an email report will be sent to your account. Wait for some time and then check if it has arrived.");
	setCursor(QCursor (Qt::WaitCursor));

	CreateHeaderTxt(ui.txtEmailReceiver->text(), "WinPerc Test");
	CreateSendMainIni(ui.txtEmailSmtp->text(), ui.txtEmailPort->text(), ui.txtEmailUsername->text(), ui.txtEmailPassword->text());
	CreateMegaCLI();
	CreateSendMail();
	CreateCmd(QDir::currentPath());

	QProcess process;

	process.start("cmd.exe", QStringList() << "/C" << "report.cmd");
	process.waitForStarted(1000);
	if (!process.waitForFinished(20000) ) {
		ShowMsg("report.cmd failed to run. Unknown error. :p", MSG_ERROR);
	}
	ui.btnTestEmailReport->setText("Test Email+Report");

	CleanFiles(QStringList() << "sendmail.exe" << "header.txt" << "sendmail.ini" << "ssleay32.dll" << 
		"libeay32.dll" << "report.cmd" << "MegaCli.exe" << "MegaSAS.log");
	setCursor(QCursor (Qt::ArrowCursor));
}

void WinPerc::OnTestEmail()
{
	ui.btnEmailTest->setText("Please wait...");
	ui.btnEmailTest->update();

	setCursor(QCursor (Qt::WaitCursor));
	ShowMsg("Now an email will be sent to your account. Wait for some time and then check if it has arrived.");

	CreateHeaderTxt(ui.txtEmailReceiver->text(), "WinPerc Test");
	CreateSendMail();
	CreateSendMainIni(ui.txtEmailSmtp->text(), ui.txtEmailPort->text(), ui.txtEmailUsername->text(), ui.txtEmailPassword->text());

	QProcess process;

	process.start("cmd.exe", QStringList() << "/C" << "sendmail.exe" << "header.txt");
	process.waitForStarted(1000);
	if (!process.waitForFinished(20000) ) {
		ShowMsg("Sendmail failed to run. Unknown error. :p", MSG_ERROR);
	}
	ui.btnEmailTest->setText("Test settings");
	CleanFiles(QStringList() << "sendmail.exe" << "header.txt" << "sendmail.ini" << "ssleay32.dll" << "libeay32.dll");
	setCursor(QCursor (Qt::ArrowCursor));
}

void WinPerc::OnTestReport()
{
	setCursor(QCursor (Qt::WaitCursor));
	CreateMegaCLI();

	QProcess process;

	process.start("cmd.exe", QStringList() << "/C" << "MegaCli.exe" << m_reportCmd << ">>" << "result.txt");
	process.waitForStarted(1000);
	if (!process.waitForFinished(20000) ) {
		ShowMsg("MegaCli.exe failed to run. Unknown error. :p", MSG_ERROR);
	}
	//print results to result textbox
	QFile file("result.txt");

	if (!file.isOpen()) {
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			ShowMsg("Could not open report text. Run as administrator may solve this problem?",
				MSG_ERROR);
			qDebug() << "Error: " << file.errorString();
			file.close();
		}
		else {
			QTextStream rs(&file);	//read stream
			QString buffer = rs.readAll();
			ui.txtReportResult->setPlainText(buffer);
			file.close();
		}
	}
	CleanFiles(QStringList() << "result.txt" << "MegaCli.exe" << "MegaSAS.log");
	setCursor(QCursor (Qt::ArrowCursor));
}