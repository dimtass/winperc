#ifndef WINPERC_H
#define WINPERC_H

#include <QtGui/QDialog>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QProcess>
#include <QMessageBox>
#include <QStringList>
#include <QTextStream>
#include <QTimer>
#include "ui_winperc.h"

class WinPerc : public QDialog
{
	Q_OBJECT

public:
	WinPerc(QWidget *parent = 0, Qt::WFlags flags = 0);
	~WinPerc();

private:
	enum {
		TAB_INTRO,
		TAB_ACTION,
		TAB_NEW_REPORT,
		TAB_SCHEDULER,
		TAB_REMOVE,
		TAB_END
	};
	typedef enum {
		MSG_INFO,
		MSG_WARNING,
		MSG_ERROR
	} en_messageType;

	void Init();
	bool CreateCmd(QString cmdPath);
	bool CreateMegaCLI(void);
	bool CreateSendMail(void);
	bool CreateHeaderTxt(QString emailTo, QString emailSubj);
	bool CreateSendMainIni(QString smtpServer, QString smptPort, QString username, QString password);
	bool CleanFiles(QStringList files);
	QStringList GetCurrentScheduleTasks(void);
	void UpdateScheduleTasks();
	bool FileCopy(QString sourceFileName, QString destFileName);
	bool DeleteFile(QString fileName);
	bool DeleteFolder(QString folderName);
	QStringList FindInstalledComponents(void);
	void UpdateInstalledComponents();
	//void TestEmail();
	//void DeleteTempDirectory();
	int m_tabIndex;
	QString m_reportCmd;
	Ui::WinPercClass ui;
	void ShowMsg(QString msg, en_messageType type = MSG_INFO);
	
public slots:
	void OnNextTab();
	void OnPrevTab();
	void OnTestEmail();
	void OnTestReport();
	void OnTestEmailReport();
	void OnSelectReportType(QString);
	void OnOpenScheduler();
	void OnDeleteSelectedTask();
	void OnCreateScedulerTask();
	void OnHideStatusGroup();
	void OnDeleteFiles();
};

#endif // WINPERC_H
